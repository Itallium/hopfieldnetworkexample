package ru.itallium.hne;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Itallium on 04.03.2017.
 */

public class NeuralNetwork {

	private List<Neural> neurals;

	public NeuralNetwork() {
		System.out.println("Create Neural Network");
		this.neurals = new ArrayList<>();
		for (File file : new File(Main.INIT_FOLDER).listFiles()) {
			System.out.println("Add new Neural from file " + file.getName());
			this.neurals.add(new Neural(file));
			System.out.println("New Neural is add");
		}
	}

	public List<Neural> getNeurals() {
		return this.neurals;
	}

	public boolean removeNeurals(Collection<Neural> neurals) {
		if (this.neurals.containsAll(neurals)) {
			return neurals.removeAll(neurals);
		} else {
			return false;
		}
	}

}
