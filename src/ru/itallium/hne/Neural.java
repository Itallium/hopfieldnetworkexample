package ru.itallium.hne;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Itallium on 04.03.2017.
 */

public class Neural {

	private String name;
	private int[] entity;
	private int lineLength;

	/**
	 * Create new Neural object from file
	 * @param file - squared bmp picture
	 */
	public Neural(File file) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(file);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		if (image == null) {
			throw new NullPointerException("Image is null");
		}
		//Transform image to int vector
		int[] imageVector = new int[image.getHeight() * image.getWidth()];
		for (int i = 0; i < image.getHeight(); i++) {
			for (int j = 0; j < image.getWidth(); j++) {
				int pixelColor = image.getRGB(j, i);
				if (pixelColor == Main.BLACK) {
					pixelColor = 1;
				} else if (pixelColor == Main.WHITE) {
					pixelColor = -1;
				}
				imageVector[(image.getHeight() * i) + j] = pixelColor;
			}
		}
		this.name = file.getName();
		this.entity = imageVector;
		this.lineLength = image.getHeight();
		System.out.println(this);
	}

	public int[] getEntity() {
		return entity;
	}

	public int getLineLength() {
		return this.lineLength;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		String s = new String();
		s += "--------\n";
		for (int i = 0; i < entity.length; i++) {
			if (i % lineLength == 0 && i != 0) {
				s += "\n";
			}
			if (entity[i] == 1) {
				s += " " + entity[i];
			} else {
				s += entity[i];
			}
		}
		s += "\n--------";
		return s;
	}
}
