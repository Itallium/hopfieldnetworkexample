package ru.itallium.hne;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Itallium on 04.03.2017.
 */

public class Main {

	public final static String INIT_FOLDER = "res/init/";
	public final static String INPUT_FOLDER = "res/input/";
	public final static String OUTPUT_FOLDER = "res/output/";

	public final static int BLACK = -16777216;
	public final static int WHITE = -1;

	public static void main(String[] args) {
		NeuralNetwork neuralNetwork = new NeuralNetwork();
		if (neuralNetwork == null) {
			throw new NullPointerException("Neural network is null");
		}
		int[][] summaryMatrix = createSummaryMatrix(neuralNetwork);
		for (File file : new File(INPUT_FOLDER).listFiles()) {
			System.out.println("Read input Neural from file " + file.getName());
			Neural inputNeural = new Neural(file);
			System.out.println("Input Neural is read");
			//Identification cycle
			int[] resInput = inputNeural.getEntity().clone();
			for (int i = 0; i < 5; i++) {
				resInput = identificationMethod(resInput.clone(), summaryMatrix);
				Collection<Neural> neuralsForRemove = new ArrayList<>();
				for (Neural neural : neuralNetwork.getNeurals()) {
					if (isVectorsEquals(neural.getEntity(), resInput)) {
						saveImageFromVector(resInput, OUTPUT_FOLDER + i + "_" + neural.getName() + "_" + file.getName() + "_success.bmp");
						neuralsForRemove.add(neural);
					} else {
						//Invert vector and try to identify
						int[] invertResInput = new int[resInput.length];
						for (int j = 0; j < invertResInput.length; j++) {
							invertResInput[j] = -resInput[j];
						}
						if (isVectorsEquals(neural.getEntity(), invertResInput)) {
							saveImageFromVector(invertResInput, OUTPUT_FOLDER + i + "_" + neural.getName() + "_" + file.getName() + "_success_invert.bmp");
							neuralsForRemove.add(neural);
						} else {
							saveImageFromVector(resInput, OUTPUT_FOLDER + i + "_" + neural.getName() + "_" + file.getName() + "_fault.bmp");
						}
					}
				}
				if (neuralsForRemove.size() > 0) {
					neuralNetwork.removeNeurals(neuralsForRemove);
					break;
				}
			}
		}
	}

	/**
	 * Multiply vector on transposable vector
	 * @param vector - input vector for calculating neural matrix
	 * @return - neural matrix
	 */
	private static int[][] getNeuralMatrix(int[] vector) {
		int[][] matrix = new int[vector.length][vector.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = vector[i] * vector[j];
			}
		}
		return matrix;
	}

	/**
	 * Calculate summary matrix W
	 * @param neuralNetwork - neural network that contains collection of neural
	 * @return - summary matrix W
	 */
	private static int[][] createSummaryMatrix(NeuralNetwork neuralNetwork) {
		System.out.println("Create Summary Matrix W");
		int sumMatrixSize = neuralNetwork.getNeurals().get(0).getLineLength() * neuralNetwork.getNeurals().get(0).getLineLength();
		int[][] returnMatrix = new int [sumMatrixSize][sumMatrixSize];
		for (Neural neural : neuralNetwork.getNeurals()) {
			int[][] neuralMatrix = getNeuralMatrix(neural.getEntity());
			for (int i = 0; i < returnMatrix.length; i++) {
				for (int j = 0; j < returnMatrix.length; j++) {
					returnMatrix[i][j] += neuralMatrix[i][j];
				}
			}
		}
		return returnMatrix;
	}

	/**
	 * Compare two vectors by elements
	 * @param firstVector - first vector to compare
	 * @param secondVector - second vector to compare
	 * @return
	 */
	private static boolean isVectorsEquals(int[] firstVector, int[] secondVector) {
		float incorrectFields = 0;
		for (int i = 0; i < firstVector.length; i++) {
			if (firstVector[i] != secondVector[i]) {
				incorrectFields += 100 / firstVector.length;
			}
		}
		if (incorrectFields != 0) {
			return false;
		}
		return true;
	}

	/**
	 * Method that multiply summary matrix W with input vector that needs the identification
	 * and transform all elements result vector to bipolar type (1 or -1)
	 * @param vector - input vector that needs to identify
	 * @param summaryMatrix - summary matrix W
	 * @return - result vector in bipolar view (each elements should be 1 or -1)
	 */
	public static int[] identificationMethod(int[] vector, int[][] summaryMatrix) {
		int[] identVector = new int[vector.length];
		for (int i = 0 ; i < summaryMatrix.length; i++) {
			for (int j = 0; j < summaryMatrix.length; j++) {
				identVector[i] += vector[j] * summaryMatrix[i][j];
			}
			identVector[i] = identVector[i] >= 0 ? 1 : -1;
		}
		return identVector;
	}

	/**
	 * Save vector to disk as squared bmp picture with side size = sqrt(input vector)
	 * @param vector - input vector that needs to save
	 * @param fileName - full file name
	 */
	private static void saveImageFromVector(int[] vector, String fileName) {
		int squareSize = (int)Math.sqrt(vector.length);
		BufferedImage outputImage = new BufferedImage(4, 4, BufferedImage.TYPE_BYTE_BINARY);
		for (int i = 0; i < squareSize; i++) {
			for (int j = 0; j < squareSize; j++) {
				if (vector[(i * squareSize) + j] >= 0) {
					outputImage.setRGB(j, i, BLACK);
				} else {
					outputImage.setRGB(j, i, WHITE);
				}
			}
		}
		try {
			ImageIO.write(outputImage, "bmp", new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}